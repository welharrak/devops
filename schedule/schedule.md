## 28 Abril
- Buscar informació sobre CI/CD Devops
- Fer exemples per entendre funcionament

## 1 Maig
- Instal·lació de jenkins a una màquina AWS (Server Jenkins)

## 2 Maig
- Prova de connectar JENKINS - GITHUB - DOCKER

## 3 Maig
- He aconseguit crear una imatge docker automaticament cada vegada que he fet push, amb jenkins.

## 5 Maig
- Una vegada creada la imatge docker, he aconseguit que posi en marcha el container cada vegada al fer push(git). (Exemple Hello-World)

## 7 Maig 
- He buscat informació sobre els Jenkinsfile

## 9 Maig
- He pogut crear un job/entorn que utilitzant un Jenkinsfile, crea la imatge docker i la posa en marcha, cada vegada que hi hagi un canvi al repositori Git.

## 10 Maig
- Buscant un monitor compatible amb Jenkins i amb Fedora 29
- He intentat instal·lar Nagios, pero no es compatible amb Fedora29
- Finalment, he optat per instal·lar un plugin, Build Monitor View, per tenir un monitor on poder controlar els builds

## 11 Maig
- He intentat instal·lar ELK Stack, pero no hi habia prou espai a la màquina AWS
- He documentat els problemes que m'he anat trobant durant el projecte

## 12 Maig
- He afegit un exemple de com seria un build amb maven

## 13 Maig
- He intentat afegir un exemple amb Node.js i una react App, pero ho he deixat estar ja que m'he trobat amb molts errors, i no val la pena seguir perdent el temps.

## 14 Maig
- Avui he estat buscant mes exemples per mostrar el funciomanet de Jenkins i alguna eina per complementar Jenkins

## 15 Maig
- He començat a fer la documentació del treball. Explicació dels exemples, instal·lació, processos,...

## 16 Maig
- He continuat la documentació. He explicat els pasos i el process dels exemples(hello-world, ldapserver i maven).

## 17 Maig
- He fet el poster i he començat amb la presentació.

## 18 Maig
- He continuat amb la presentació. I he fet esquemes de la meva infraestructura i d'un exemple
