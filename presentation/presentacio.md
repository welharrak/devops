### Abans de començar...

![CI/CD](./../aux/ci-cd.png)

### Tan eficaç és?

- Lliurar o alliberar codi de manera immediata, sense esperar a acumular moltes funcionalitats

- Menor esforç al moment de desplegar codi a l'etapa de producció

- Es poden identificar errors abans de publicar el codi, amb més rapidesa

### Pipeline

- Conjunt de "Stages/Steps"

- Base de l'automatització

- Opcions il·limitades

![Pipeline](./../aux/pipeline.png)

### DevOps

- És una **metodologia** de treball

- Es basa en la integració entre desenvolupadors de programari(Dev) i administradors de sistemes(Ops)

- Permet fabricar programari més ràpidament, amb major qualitat, menor cost i una altíssima freqüència de releases

- Automatització i el monitoratge en tots els passos de la construcció del programari

- Fomenta el treball en equip

### Llavors DevOps i CI/CD són el mateix?

- No! Però casi

![DevOps](./../aux/devops.jpg)

### Eines

- Jenkins (+ plugins)

- Docker

- Nagios (X)

- ELK Stack (X)

### Docker

- Automatitza el desplegament d'aplicacions dins de contenidors

![Docker](./../aux/docker.png)

### Nagios

- Sistema de monitoratge de xarxes, que vigila els equips (maquinari) i serveis (programari) que s'especifiquin

- Alerta quan els equips o els serveis fallen

![Nagios](./../aux/nagios.jpg)

### ELK Stack

- Elasticsearch: motor de cerca i analítica

- Logstash: processa les dades

- Kibana: permet als usuaris visualitzar les dades

![ELKStack](./../aux/elkstack.png)

### Jenkins

- Servidor d'automatització

- Test

- Build

- Deploy

![Jenkins](./../aux/jenkins.png)

### Plugins
- Components per ampliar la funcionalitat de Jenkins

- GitHub plugin

- BlueOcean plugin

- Build Monitor View plugin

- Docker plugin

### Jenkinsfile

- Pipline de Jenkins

![Jenkinsfile](./../aux/jenkinsfile.png)

### Com ho tinc muntat?

![Infraestructura](./../aux/esquema-general.png)

### Exemple

![Hello-world](./../aux/esquema-helloworld.png)

### Problemes

- Nagios  

![Problem-Nagios](./../aux/problem-nagios.png)

###

- ELK Stack  

![Problem-ELKStack](./../aux/problem-ELKStack.png)

### I això és tot!!
