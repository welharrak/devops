# Problems
## Problems that I had during the project

- **Nagios: fedora29 is not currently supported**
![Problem-Nagios](./../aux/problem-nagios.png)

- **ELK Stack: error: Couldn't fork %prein(elasticsearch-0:7.6.2-1.x86_64): Cannot allocate memory**
![Problem-ELKStack](./../aux/problem-ELKStack.png)
