## Docker - Jenkins - Git
- https://www.stratoscale.com/blog/devops/practical-devops-use-case-github-jenkins-docker/
- https://www.youtube.com/watch?v=nMLQgXf8tZ0
- https://www.youtube.com/watch?v=6tcoRIPBd8s&t=876s
- https://medium.com/@angellom/automatically-building-a-flask-docker-image-on-git-push-with-jenkins-5a30c9fc9beb
- https://www.katacoda.com/courses/jenkins/build-docker-images
- https://blog.nearsoftjobs.com/construir-probar-y-lanzar-aplicaciones-usando-solo-jenkins-y-docker-6c2936d8f59a

## Install Jenkins
- https://www.jenkins.io/doc/book/installing/

## Install Docker
- https://docs.docker.com/engine/install/fedora/

## Jenkinsfile - Pipelines
- https://www.jenkins.io/doc/pipeline/tour/hello-world/
- https://www.jenkins.io/doc/book/pipeline/jenkinsfile/
- http://oscarmoreno.com/pipeline-jenkins/
- https://www.jenkins.io/doc/book/pipeline/
- https://www.paradigmadigital.com/dev/pipelines-de-jenkins-evolucion-continuous-delivery/

## ELK Stack
- https://logz.io/blog/jenkins-elk-stack/

## Build Monitor
- https://www.youtube.com/watch?v=WnQK6-puXSM

## Nagios
- https://www.nagios.com/downloads/nagios-xi/linux/
- https://assets.nagios.com/downloads/nagiosxi/docs/Installing-Nagios-XI-Manually-on-Linux.pdf

## Maven project
- https://www.jenkins.io/doc/tutorials/build-a-java-app-with-maven/
