# Maven jenkins example
For this we will need a git repository with the code to create this basic example. Ex: https://github.com/welharrak/maven-example

## Steps
- First of all, we have to create a new job, and choose the **Pipeline** option:  
![NewJob](../../aux/maven1.png)

- [Optional]**General**: We can write a description of the project and the Git repository.  
![General](../../aux/maven2.png)

- **Build Triggers**: In this section, we choose **Poll SCM** and write the following schedule: ``` * * * * *```. This is going to tell to he Job that every minute have to see if there are changes in the Git repository, and if there are changes, have to do the build.  
![Build](../../aux/maven3.png)

- **Pipeline**: Here we will set the source of the Pipeline (Jenkinsfile), we have to choose Git as SCM and write the **Git url**, not the web url, this is very important.
![Pipeline](../../aux/maven4.png)

- And that's it! We have created a maven project. Now we are going to see the process.

## Process (Pipeline)
- The Jenkinsfile is the Jenkins Pipeline, and contains the stages of the build and the steps that have to do to complete the build.

- In this case, we have the following [Jenkinsfile](./Jenkinsfile):

1. Firstly, we establish our agent, that is going to be Docker, and write the image that we are going to use.
```
agent {
        docker {
            image 'maven:3-alpine'
            args '-v /root/.m2:/root/.m2'
        }
    }
```

2. Also, we can add an option, **skipStagesAfterUnstable()**, that skips stages once the build status has gone to **unestable**.
```
options {
        skipStagesAfterUnstable()
    }
```

3. In the first satge, runs a Maven command to cleanly build your Java application (without running any tests).
```
stage('Build') {
          steps {
                sh 'mvn -B -DskipTests clean package'
            }
        }
```

4. The second stage, it's for testing. Executes the Maven command to run the unit test on your simple Java application. This command also generates a JUnit XML report, which is saved to the target/surefire-reports directory.
```
stage('Test') {
            steps {
                sh 'mvn test'
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }
```

5. The last stage is to Deliver the build. Runs the shell script [deliver.sh](./jenkins/scripts/deliver.sh) located in the jenkins/scripts.(Explanations about what this script does are covered in the deliver.sh file itself.)
```
stage('Deliver') {
            steps {
                sh './jenkins/scripts/deliver.sh'
            }
        }
```
