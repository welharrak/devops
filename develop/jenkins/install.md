# Jenkins
## Jenkins Server in AWS
- Install jenkins in a AWS machine and other packages that we will need in the future

```
sudo dnf install wget git -y

sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo

sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

sudo dnf install jenkins java java-devel -y
```

## Start service
```
sudo systemctl start jenkins

sudo systemctl enable jenkins
```

## Configuration
- Once we have installed Jenkins, we'll need to go to http://localhost:8080 or http://our-ip/8080

- To get started to configure Jenkins, is going to ask us a password that is located in ```/var/jenkins_home/secrets/initialAdminPassword```:
![Password](./../../aux/foto1.png)

- Then we are going to select manually the plugins that we will need:
1. Docker
2. Git
3. GitHub
4. Open Blue Ocean

- Create the Admin User:  
![AdminUser](./../../aux/foto2.png)

- And we got it!


*For more information:* [https://www.jenkins.io/doc/book/installing/](https://www.jenkins.io/doc/book/installing/)
